import './App.css';
import { useState, useEffect } from 'react';
import axios from 'axios';
import Navbar from './components/Navbar';


const App = () => {
  const API_KEY = "1be31a033d29d622844527b5dd0956e5";

  const [ requestToken, setRequestToken ] = useState('');
  const [ authToken, setAuthToken ] = useState('');
  const [ sessionId, setSessionId ] = useState('');

  const getToken = async() => {
    const response = await axios.get(`https://api.themoviedb.org/3/authentication/token/new?api_key=${API_KEY}`);
    setRequestToken(response.data.request_token);
  };

  const createSession = async() => {
    await axios
      .post(`https://api.themoviedb.org/3/authentication/token/validate_with_login?api_key=${API_KEY}`, {
        username: 'grakhimbayeva',
        password: '852456',
        request_token: requestToken
      })
      .then(res => setAuthToken(res.data.request_token))
  }

  const getSessionID = async() => {
    await axios
      .post(`https://api.themoviedb.org/3/authentication/session/new?api_key=${API_KEY}`, {
        request_token: authToken
      }, { headers: {
        "Accept": 'application/json',
        "Content-Type": 'application/json',
        "Authorization": API_KEY
      }})
      .then(res=>setSessionId(res.data.session_id))
      .catch(err=>console.log(err));
  }

  useEffect(()=>{
    getToken();
    createSession();
    getSessionID();
  }, []);

  return (
    <div className="App">
      <Navbar />
     <h1>Start Anew</h1>
     <p>{sessionId}</p>
    </div>
  );
}

export default App;
