import React from 'react';

const Navbar = () => {
  return (
    <div className='navbar'>
        <div className='leftside'>
            <button>Home</button>
        </div>
        <div className='rightside'>
            <div className='not_signed_in'>
                <button>Sign In</button>
            </div>
            <div className='signed_in'>
                <span className='username'>User Name</span>
            </div>
        </div>
    </div>
  )
}

export default Navbar;